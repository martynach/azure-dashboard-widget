import Litepicker from 'litepicker';

export class DateRangeService {

    private picker: Litepicker;

    private onSelect: Function;

    constructor(onSelect: Function, defaultStartDate: number | Date | string, defaultEndDate: number | Date | string) {
        this.onSelect = onSelect;
        const element = document.getElementById('datepicker');
        element.hidden = false;
        this.picker = new Litepicker({
            element: element,
            singleMode: false,
            numberOfMonths: 2,
            numberOfColumns: 2,
            onSelect: this.dateSelected.bind(this),
            startDate: defaultStartDate,
            endDate: defaultEndDate
        });
    }


    private dateSelected(startDate: Date, endDate: Date) {
        console.log('onSelect startDate: ', startDate);
        console.log('onSelect endDate: ', endDate);
        this.onSelect(startDate, endDate);

    }




}