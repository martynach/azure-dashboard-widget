import {CycleTimeInfo} from "../models/ui/CycleTimeInfo";

// Optionally use Azure storage
//  https://docs.microsoft.com/en-us/azure/devops/extend/develop/data-storage?view=azure-devops
export class DataStorageWrapperService {

    private cycleTimeDataCollectionName = 'CYCLE_TIME_DATA';
    private existingIdsCollectionName = 'EXISTING_IDS';
    private _isLoading: boolean = false;

    constructor(private _dataService) {

        console.log('_dataService: ');
        console.log(_dataService);

    }

    public async updateDataStorageWithNewestData() {
        this._isLoading = true;

        this._isLoading = false;
    }


    public isLoading(): boolean {
        return this._isLoading;
    }

    getDocumentIdForCurrentDate(): string {
        return '';

    }

    getPreviousDocumentId(currentDocumentId: string): string {
        const currentDocumentIdNumber: number = parseInt(currentDocumentId, 10);
        if(currentDocumentId.endsWith('1')) {
            return '' + (currentDocumentIdNumber - 100 + 11);
        }

        return '' + (currentDocumentIdNumber - 1);

    }

    createOrUpdateCurrentMonthDocument(): void {

    }

    sortExistingIds(existingIds: string[]): string[] {
        const existingIdsNumber: number[] = existingIds.map((id: string) => parseInt(id, 10));
        const sortedIdsNumber: number[] = existingIdsNumber.sort(); // todo check if sorts correctly
        console.log('sorted ids: ', sortedIdsNumber);
        return sortedIdsNumber.map((idNumber: number) => '' + idNumber);
    }




}


