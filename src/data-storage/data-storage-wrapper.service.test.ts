import {DataStorageWrapperService} from "./data-storage-wrapper.service";

test('Test getPreviousDocumentId', () => {
    const dataStorageWrapperService = new DataStorageWrapperService(null);
    expect(dataStorageWrapperService.getPreviousDocumentId("202009")).toBe("202008");
    expect(dataStorageWrapperService.getPreviousDocumentId("202012")).toBe("202011");
    expect(dataStorageWrapperService.getPreviousDocumentId("202001")).toBe("201912");
    expect(dataStorageWrapperService.getPreviousDocumentId("201912")).toBe("201911");
    expect(dataStorageWrapperService.getPreviousDocumentId("201901")).toBe("201812");
});

test('Test sortExistingIds', () => {
    const dataStorageWrapperService = new DataStorageWrapperService(null);
    const existingIds = ['202009', '202012', '202001', '201912', '201901'];
    const resultSortedExistingIds = dataStorageWrapperService.sortExistingIds(existingIds);

    const expectedSortedExistingIds = ['202012', '202009','202001', '201912', '201901'];

    expect(resultSortedExistingIds[0]).toBe(expectedSortedExistingIds[0]);
    expect(resultSortedExistingIds[0]).toBe(expectedSortedExistingIds[1]);
    expect(resultSortedExistingIds[0]).toBe(expectedSortedExistingIds[2]);
    expect(resultSortedExistingIds[0]).toBe(expectedSortedExistingIds[3]);
    expect(resultSortedExistingIds[0]).toBe(expectedSortedExistingIds[4]);

});