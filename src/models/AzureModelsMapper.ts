import {EnvironmentDeploymentRecord} from "./ui/EnvironmentDeploymentRecord";
import {OriginalPullRequest} from "./azure/OriginalPullRequest";
import {OriginalEnvironmentDeploymentRecord} from "./azure/OriginalEnvironmentDeploymentRecord";
import {PullRequestInfo} from "./ui/CycleTimeInfo";

export class AzureModelsMapper {
    public static mapAzureEnvironmentDeploymentRecordToDeploymentRecord(originalRecord: OriginalEnvironmentDeploymentRecord): EnvironmentDeploymentRecord {
        return {
            id: originalRecord.id,
            definitionName: originalRecord.definition.name,
            buildDefinitionUrl: originalRecord.definition._links.self.href,
            jobStartTime: originalRecord.startTime,
            jobFinishTime: originalRecord.finishTime,
            jobName: originalRecord.jobName,
            environmentId: originalRecord.environmentId,
            stageName: originalRecord.stageName,
            buildChangesUrl: originalRecord.owner._links.self.href + '/changes',
            buildId: originalRecord.owner.id,
            buildName: originalRecord.owner.name
        }
    }

    public static mapAzurePullRequestToPullRequestInfo(originalPullRequest: OriginalPullRequest): PullRequestInfo {
        return {
            message: originalPullRequest.completionOptions.mergeCommitMessage,
            completionDate: originalPullRequest.closedDate,
            mergeCommitId: originalPullRequest.lastMergeCommit.commitId
        }
    }

}