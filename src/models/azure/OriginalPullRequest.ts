export interface OriginalPullRequest {
    pullRequestId: string;
    closedDate: string;
    creationDate: string;
    completionOptions: {
        mergeCommitMessage: string;
    }
    title: string;
    lastMergeCommit: {
        commitId: string;
    }
}