export interface OriginalEnvironmentDeploymentRecord {
    id: number;
    definition: {
        name: string;
        _links: {
            self: {
                href: string
            }
        }
    };
    startTime: string;
    finishTime: string;
    jobName: string;
    environmentId: number;
    stageName: string;
    owner: {
        id: number;
        name: string;
        _links: {
            self: {
                href: string;
            }
        }
    }
}