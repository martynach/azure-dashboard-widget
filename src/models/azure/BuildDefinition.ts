export interface BuildDefinition {
    path: string;
    repository: {
        id: string;
        name: string;
    }
}