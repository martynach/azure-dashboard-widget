export interface GitCommitRef {
    commitId: string;
    committer: {
        date: string;
    }
    statuses: Status[];
}

export interface Status {
    targetUrl: string;
}