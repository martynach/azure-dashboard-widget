export interface PullRequest {
    id: string;
    message: string;
    location?: string;
    timestamp: string;
}