export interface EnvironmentDeploymentRecord {
    id: number;
    definitionName: string;
    buildDefinitionUrl: string;
    repositoryId?: string;
    previousBuildStartTime?: string;
    buildStartTime?: string;
    repositoryName?: string;
    jobStartTime: string;
    jobFinishTime: string;
    jobName: string;
    environmentId: number;
    stageName: string;
    buildChangesUrl: string;
    buildId: number;
    buildName: string;
}