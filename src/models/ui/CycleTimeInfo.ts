export interface CycleTimeInfo {
    deploymentDefinitionName: string;
    deploymentDate: string;
    pullRequestsInfo: PullRequestInfo[];
}

export interface PullRequestInfo {
    completionDate: string;
    mergeCommitId: string;
    message: string;
    location?: string;
}

// export interface CachedCircleTimeInfo {
//     year: number;
//     month: number;
//     id: "year" + "month"
//     data: CycleTimeInfo[];
// }