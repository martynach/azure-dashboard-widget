import {CycleTimeInfo, PullRequestInfo} from "../models/ui/CycleTimeInfo";
import Chart, {ChartColor, ChartLayoutPaddingObject, ChartType} from "chart.js";

export class ChartDrawingService {

    private millisecondsInOneDay: number = 1000 * 60 * 60 * 24;
    private millisecondsInOneHour: number = 1000 * 60 * 60;
    private millisecondsInOneMinute: number = 1000 * 60;

    private chart: Chart;

    public updateChartWithDeploymentDatesOnXAxis(cycleTimeInfo: CycleTimeInfo[]) {
        const datasets: ChartDataset[] = this.transformDataIntoChartWithDeploymentDatesOnXAxeFormat(cycleTimeInfo);
        this.chart.data.datasets = datasets;
        this.chart.update();

    }

    public createChartWithDeploymentDatesOnXAxis(cycleTimeInfo: CycleTimeInfo[]) {
        console.log('Creating chart with deployment dates on X');
        const datasets: ChartDataset[] = this.transformDataIntoChartWithDeploymentDatesOnXAxeFormat(cycleTimeInfo);
        console.log('datasets: ', datasets);
        const ctx = (document.getElementById('deploymentBasedChart') as HTMLCanvasElement).getContext('2d');

        try {
            this.chart = new Chart(ctx, {
                type: 'scatter',
                data: {
                    datasets: datasets
                },
                options: {
                    scales: {
                        xAxes: [{
                            type: 'time',
                            position: 'bottom',
                            time: {
                                unit: 'day'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Deployment dates'
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Time between PR completion and deployment (in days)'
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: tooltipItem => {
                                return datasets[tooltipItem.datasetIndex].data[tooltipItem.index].tooltip;
                            },
                        }

                    },
                    maintainAspectRatio: false
                }
            });
        } catch (error) {
            console.error('ERROR!!! ');
            console.error(error);
        }
    }

    private transformDataIntoChartWithDeploymentDatesOnXAxeFormat(cycleTimeInfo: CycleTimeInfo[]): ChartDataset[] {
        const datasets: ChartDataset[] = [];
        const existingDeploymentNames: string[] = [];

        cycleTimeInfo.forEach((deploymentInfo: CycleTimeInfo) => {

            const chartData: ChartDataProperty[] = deploymentInfo.pullRequestsInfo.map((pullRequestsInfo: PullRequestInfo) => {

                let yValue = (new Date(deploymentInfo.deploymentDate).getTime() - new Date(pullRequestsInfo.completionDate).getTime()) / (this.millisecondsInOneDay);
                let isError = false;


                if( isNaN(yValue)) {
                    console.log('Got error when creating chart; y is NAN');
                    isError = true;
                    yValue = 0;
                }

                return {
                    x: new Date(deploymentInfo.deploymentDate),
                    y: yValue,
                    tooltip: this.getTooltipForPullRequest(pullRequestsInfo, deploymentInfo, isError)
                };
            });

            const deploymentIndex = existingDeploymentNames.indexOf(deploymentInfo.deploymentDefinitionName);
            if (deploymentIndex === -1) {
                const color = this.getRandomColor();
                datasets.push({
                    label: deploymentInfo.deploymentDefinitionName,
                    data: chartData,
                    backgroundColor: color,
                    borderColor: color,
                    type: 'scatter',
                    fill: false
                });
                existingDeploymentNames.push(deploymentInfo.deploymentDefinitionName);
            } else {
                datasets[deploymentIndex].data.push(...chartData);
            }
        });
        return datasets;
    }

    private getRandomColor(): string {
        const r = Math.floor(Math.random() * 255);
        const g = Math.floor(Math.random() * 255);
        const b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }

    private transformDataIntoChartWithPullRequestDatesOnXAxisFormat(cycleTimeInfo: CycleTimeInfo[]): ChartDataset[] {
        const datasets: ChartDataset[] = [];
        const existingDeploymentNames: string[] = [];

        cycleTimeInfo.forEach((deploymentInfo: CycleTimeInfo) => {
            const chartData: ChartDataProperty[] = deploymentInfo.pullRequestsInfo.map((pullRequestInfo: PullRequestInfo) => {

                const yValue = (new Date(deploymentInfo.deploymentDate).getTime() - new Date(pullRequestInfo.completionDate).getTime()) / (this.millisecondsInOneDay);
                return {
                    x: new Date(pullRequestInfo.completionDate),
                    y: yValue,
                    tooltip: this.getTooltipForPullRequest(pullRequestInfo, deploymentInfo, false)
                };
            });

            const deploymentIndex = existingDeploymentNames.indexOf(deploymentInfo.deploymentDefinitionName);
            if (deploymentIndex === -1) {
                const color = this.getRandomColor();
                datasets.push({
                    label: deploymentInfo.deploymentDefinitionName,
                    data: chartData,
                    backgroundColor: color,
                    borderColor: color,
                    type: 'line',
                    fill: false
                });
                existingDeploymentNames.push(deploymentInfo.deploymentDefinitionName);
            } else {
                datasets[deploymentIndex].data.push(...chartData);
            }
        });

        return datasets;

    }

    private getTooltipForPullRequest(pullRequest: PullRequestInfo, deploymentInfo: CycleTimeInfo, isError: boolean): string[] {
        const deploymentDate = new Date(deploymentInfo.deploymentDate);
        const timeBetweenPRCompletionAndDeployment: number = deploymentDate.getTime() - new Date(pullRequest.completionDate).getTime();

        if(isError) {
            return [
                'ERROR',
                'Error when obtaining PR for ' + deploymentInfo.deploymentDefinitionName,
                'Deployment date: ' + deploymentDate.toLocaleString(),
            ];
        }

        return [
            pullRequest.message,
            'PR completion date: ' + new Date(pullRequest.completionDate).toLocaleString(),
            'Deployment date: ' + deploymentDate.toLocaleString(),
            'Time between PR completion and deployment: ' + this.getFormattedDayHourMinuteTimeDifference(timeBetweenPRCompletionAndDeployment)
        ];
    }

    private getFormattedDayHourMinuteTimeDifference(timeDifferenceInMilliseconds: number): string {
        const days = Math.floor(timeDifferenceInMilliseconds / this.millisecondsInOneDay);
        const hours = Math.floor(timeDifferenceInMilliseconds / this.millisecondsInOneHour);
        const minutes = Math.round(timeDifferenceInMilliseconds / this.millisecondsInOneMinute);
        let formattedTimeDifference: string = '';

        if (days > 1) {
            formattedTimeDifference += (days + ' days, ');
        }

        if (days === 1) {
            formattedTimeDifference += (days + ' day, ');
        }

        if((hours % 24) === 1) {
            formattedTimeDifference += ((hours % 24) + ' hour, ');
        } else {
            formattedTimeDifference += ((hours % 24) + ' hours, ');
        }

        formattedTimeDifference += ((minutes % 60) + ' minutes.');
        return formattedTimeDifference;
    }

    public async createChartWithPullRequestDatesOnXAxis(cycleTimeInfo: CycleTimeInfo[]) {

        const datasets: ChartDataset[] = this.transformDataIntoChartWithPullRequestDatesOnXAxisFormat(cycleTimeInfo);
        const ctx = (document.getElementById('pullRequestBasedChart') as HTMLCanvasElement).getContext('2d');


        console.log('datasets: ', datasets);

        try {
            const chart = new Chart(ctx, {
                type: 'scatter',
                data: {
                    datasets: datasets,
                },
                options: {
                    scales: {
                        xAxes: [{
                            type: 'time',
                            position: 'bottom',
                            time: {
                                unit: 'day'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PR completion dates'
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Time between PR completion and deployment (in days)'
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: tooltipItem => {
                                return datasets[tooltipItem.datasetIndex].data[tooltipItem.index].tooltip;
                            },
                        }
                    },
                    maintainAspectRatio: false
                }
            });
        } catch (error) {
            console.error('ERROR!!! ');
            console.error(error);
        }

    }

}

interface ChartDataset {
    label: string;
    data: ChartDataProperty[];
    backgroundColor: string;
    borderColor: string;
    type: ChartType;
    fill: boolean;
}

interface ChartDataProperty {
    x: Date,
    y: number,
    tooltip: string[];
}