import {CycleTimeInfo, PullRequestInfo} from "../models/ui/CycleTimeInfo";
import {EnvironmentDeploymentRecord} from "../models/ui/EnvironmentDeploymentRecord";
import {Environment} from "../models/azure/Environment";
import {IAzureDevopsRestApiService} from "./IAzureDevopsRestApiService";
import {AzureModelsMapper} from "../models/AzureModelsMapper";
import {BuildDefinition} from "../models/azure/BuildDefinition";
import {OriginalPullRequest} from "../models/azure/OriginalPullRequest";
import {OriginalEnvironmentDeploymentRecord} from "../models/azure/OriginalEnvironmentDeploymentRecord";
import {GitCommitRef} from "../models/azure/GitCommitRef";

// All Pull Requests retrieved and sorted by dates between deployments
// https://docs.microsoft.com/en-us/rest/api/azure/devops/git/pull%20requests/get%20pull%20requests?view=azure-devops-rest-6.1
export class AzureDevopsRestApiPullRequestSortedByDatesService implements IAzureDevopsRestApiService {

    private webContext: WebContext = VSS.getWebContext();
    private ALL_ENVIRONMENTS_LIST_URL = 'https://dev.azure.com/' + this.webContext.account.name + '/' + this.webContext.project.name + '/_apis/distributedtask/environments?&api-version=6.0-preview.1';

    private authorizationHeader;
    private environment: Environment;
    private ENVIRONMENT_DEPLOYMENT_RECORDS_URL;

    private pullRequestsForRepositories = new Map<string, PullRequestInfo[]>();

    private buildIdsAndCommitsInfo = new Map<number, { date: string, commitId: string }>();

    constructor(private VSS_Auth_Service, private environmentName: string) {
    }

    public async getCycleTimeInfo(endSearchDateInMilliseconds: number): Promise<CycleTimeInfo[]> {

        if (!this.authorizationHeader) {
            await this.setAuthorizationHeader();
        }

        if (!this.ENVIRONMENT_DEPLOYMENT_RECORDS_URL) {
            await this.setEnvironmentDeploymentRecordsUrl();
        }

        const environmentDeploymentRecords: EnvironmentDeploymentRecord[] =
            await this.getEnvironmentDeploymentsRecords(this.ENVIRONMENT_DEPLOYMENT_RECORDS_URL, this.authorizationHeader, endSearchDateInMilliseconds);

        await this.setRepositoryInfoForDeploymentRecords(environmentDeploymentRecords);


        try {
            const commitPromises = environmentDeploymentRecords.map(record => record.repositoryId).filter(this.filterUnique).map(async repositoryId => {
                await this.setCommitBuildIdsAndCommitsInfo(repositoryId, ''); // todo
            });

            await Promise.all(commitPromises);

        } catch (error) {
            console.log('ups spmething went wrong');
            console.error(error);
        }

        this.setBuildStartTime(environmentDeploymentRecords);

        this.setPreviousDeploymentFinishTimeForDeploymentRecords(environmentDeploymentRecords);


        return await this.transformDeploymentRecordsToCycleTimeInfo(environmentDeploymentRecords);
    }


    private filterUnique(url, index, array) {
        return array.indexOf(url) === index;
    }

    private async setCommitBuildIdsAndCommitsInfo(repositoryId: string, oldestCommit: string) {

        // const oldestCommitDate = new Date(oldestCommit).getTime();
        console.log(' - - getCommitsForRepository - - -');
        const url = 'https://dev.azure.com/msi-cie/CommunityEngagement/_apis/git/repositories/' + repositoryId + '/commitsbatch?includeStatuses=true&api-version=6.0';
        let skip = 0;

        while (true) {
            let body = {
                $skip: skip,
                historyMode: "firstParent",
                itemVersion: {
                    version: "master",
                    versionType: "branch"
                }
            };
            const responsePromise = await fetch(url, {
                method: 'post',
                body: JSON.stringify(body),
                headers: {Authorization: this.authorizationHeader, 'Content-Type': 'application/json'}
            });
            if (responsePromise.ok) {
                const commitsResponse = await responsePromise.json();
                const count = commitsResponse.count;
                if (count === 0) {
                    break;
                }
                skip += count;
                this.transformCommitsToBuildDateMap(commitsResponse.value)
                // const lastCommit: GitCommitRef = commitsResponse.value[count - 1];
                // const commitDate = new Date(lastCommit.committer.date);


                // if (commitDate.getTime() < oldestCommitDate) {
                //     break
                // }

                console.log('this.buildIdsAndCommitsInfo: ', this.buildIdsAndCommitsInfo);
            } else {
                console.log('error error'); // todo
                break;
            }
        }
        ;
    }

    private transformCommitsToBuildDateMap(commits: GitCommitRef[]) {
        console.log({commits});

        commits.forEach(commit => {
            if (!commit.statuses || commit.statuses.length === 0) {
                // console.log('No statuses found for commit: ', commit);
                return;
            }
            const commitTargetUrls: string [] = commit.statuses.map(status => status.targetUrl).filter(this.filterUnique);

            commitTargetUrls.forEach(targetUrl => {
                try {
                    const buildId = parseInt(targetUrl.replace('vstfs:///Build/Build/', ''));
                    this.buildIdsAndCommitsInfo.set(buildId, {date: commit.committer.date, commitId: commit.commitId});

                } catch (error) {
                    console.log('ups something went wrong');
                    console.log(error);
                }
            });
        });
    }

    private async transformDeploymentRecordsToCycleTimeInfo(environmentDeploymentRecords: EnvironmentDeploymentRecord[]): Promise<CycleTimeInfo[]> {
        const cycleTimeData: CycleTimeInfo[] = [];
        const pullRequestsTriggeringBuild: PullRequestInfo[] = [];
        for (let index = environmentDeploymentRecords.length - 1; index >= 0; index--) {
            let currentRecord = environmentDeploymentRecords[index];

            if (!currentRecord.repositoryId) {
                // todo find out when cannot find environment correlation with repository
                console.log('Ignoring deployment record because not found repository id: ', currentRecord);
                continue;
            }

            if (!currentRecord.previousBuildStartTime) {
                continue;
            }

            let pullRequests: PullRequestInfo[] = await this.getAndRememberInCachePullRequests(currentRecord.repositoryId);

            let endDateRangeForPullRequests: number = (new Date(currentRecord.buildStartTime)).getTime();
            let startDateRangeForPullRequests: number = (new Date(currentRecord.previousBuildStartTime)).getTime();

            let filteredPullRequestsByDate: PullRequestInfo[] = pullRequests.filter((pullRequest: PullRequestInfo) => {
                let pullRequestCompletionDate: number = (new Date(pullRequest.completionDate)).getTime();
                if (this.buildIdsAndCommitsInfo.has(currentRecord.buildId) && (pullRequest.mergeCommitId === this.buildIdsAndCommitsInfo.get(currentRecord.buildId).commitId)) {
                    pullRequestsTriggeringBuild.push(pullRequest);
                    // console.log({pullRequest});
                    return true;
                }

                // checking if PR is not triggering the earlier build
                // because the PR completion date can be a little bit later than build start time (merge commit date)
                if ((pullRequestCompletionDate <= endDateRangeForPullRequests && pullRequestCompletionDate > startDateRangeForPullRequests) &&
                    pullRequestsTriggeringBuild.includes(pullRequest)
                ) {
                    return false;
                }

                return (pullRequestCompletionDate <= endDateRangeForPullRequests && pullRequestCompletionDate > startDateRangeForPullRequests);
            });

            const cycleTimeInfo: CycleTimeInfo = {
                deploymentDefinitionName: currentRecord.definitionName,
                deploymentDate: currentRecord.jobFinishTime,
                pullRequestsInfo: filteredPullRequestsByDate
            }

            cycleTimeData.push(cycleTimeInfo);
        }

        return cycleTimeData;
    }


    private async setAuthorizationHeader() {
        const token = await VSS.getAccessToken();
        this.authorizationHeader = this.VSS_Auth_Service.authTokenManager.getAuthorizationHeader(token);
    }

    private async setEnvironment() {
        const environmentListResponse = await (await fetch(this.ALL_ENVIRONMENTS_LIST_URL, {headers: {Authorization: this.authorizationHeader}})).json();

        const expectedEnvironment: Environment = this.getExpectedEnvironment(this.environmentName, environmentListResponse.value);
        this.environment = expectedEnvironment;
    }

    private async setEnvironmentDeploymentRecordsUrl() {
        if (!this.environment) {
            await this.setEnvironment();
        }
        this.ENVIRONMENT_DEPLOYMENT_RECORDS_URL =
            'https://dev.azure.com/' + this.webContext.account.name
            + '/' + this.webContext.project.name + '/_apis/distributedtask/environments/'
            + this.environment.id + '/environmentdeploymentrecords?api-version=6.0-preview.1';
    }

    private async getAndRememberInCachePullRequests(repositoryId: string): Promise<PullRequestInfo[]> {

        if (this.pullRequestsForRepositories.has(repositoryId)) {
            return this.pullRequestsForRepositories.get(repositoryId);
        }

        const pullRequestsUrl = 'https://dev.azure.com/'
            + this.webContext.account.name + '/' + this.webContext.project.name +
            '/_apis/git/repositories/'
            + repositoryId +
            '/pullrequests?searchCriteria.status=completed&api-version=6.1-preview.1';

        const pullRequestsResponse: Response = await fetch(pullRequestsUrl, {headers: {Authorization: this.authorizationHeader}});

        const pullRequestsResponseBody = await pullRequestsResponse.json();
        const pullRequests: OriginalPullRequest[] = pullRequestsResponseBody.value;

        const pullRequestsInfo = pullRequests.map(pullRequest => AzureModelsMapper.mapAzurePullRequestToPullRequestInfo(pullRequest));

        this.pullRequestsForRepositories.set(repositoryId, pullRequestsInfo);
        return pullRequestsInfo;
    }

    private async setRepositoryInfoForDeploymentRecords(environmentDeploymentRecords: EnvironmentDeploymentRecord[]) {
        const repositoryInfoPromiseForBuildDefinitionUrl = new Map<string, Promise<any>>();

        const buildDefinitionPromises: Promise<any>[] = environmentDeploymentRecords.map((record: EnvironmentDeploymentRecord) => {
            if (repositoryInfoPromiseForBuildDefinitionUrl.has(record.buildDefinitionUrl)) {
                return repositoryInfoPromiseForBuildDefinitionUrl.get(record.buildDefinitionUrl);
            }
            const responsePromise = fetch(record.buildDefinitionUrl, {headers: {Authorization: this.authorizationHeader}});
            repositoryInfoPromiseForBuildDefinitionUrl.set(record.buildDefinitionUrl, responsePromise);
            return responsePromise;
        });


        const buildDefinitionsMap = new Map<string, BuildDefinition>();
        const buildDefinitionResponses: any [] = await Promise.all(buildDefinitionPromises);

        for (let index = 0; index < buildDefinitionResponses.length; index++) {
            let currentEnvDeplRecord = environmentDeploymentRecords[index];
            if (buildDefinitionResponses[index].ok) {
                let buildDefinition: BuildDefinition;
                if (buildDefinitionsMap.has(currentEnvDeplRecord.buildDefinitionUrl)) {
                    buildDefinition = buildDefinitionsMap.get(currentEnvDeplRecord.buildDefinitionUrl);
                } else {
                    buildDefinition = await buildDefinitionResponses[index].json();
                    buildDefinitionsMap.set(currentEnvDeplRecord.buildDefinitionUrl, buildDefinition);
                }
                currentEnvDeplRecord.repositoryId = buildDefinition.repository.id;
                currentEnvDeplRecord.repositoryName = buildDefinition.repository.name;
            } else {
                console.error('Error when retrieving repository info for: ', environmentDeploymentRecords[index]); // todo
            }
        }
    }

    private setBuildStartTime(environmentDeploymentRecords: EnvironmentDeploymentRecord[]) {
        environmentDeploymentRecords.forEach(record => {
            if (!this.buildIdsAndCommitsInfo.has(record.buildId)) {
                console.error('Did not find commit for build id: ', record.buildId);
                return;
            }
            record.buildStartTime = this.buildIdsAndCommitsInfo.get(record.buildId).date;
        });
    }

    private setPreviousDeploymentFinishTimeForDeploymentRecords(environmentDeploymentRecords: EnvironmentDeploymentRecord[]) {
        const deploymentRecordsMap: Map<string, EnvironmentDeploymentRecord[]> = new Map<string, EnvironmentDeploymentRecord[]>();

        environmentDeploymentRecords.forEach((record: EnvironmentDeploymentRecord) => {
            if (deploymentRecordsMap.has(record.definitionName)) {
                deploymentRecordsMap.get(record.definitionName).push(record);
            } else {
                deploymentRecordsMap.set(record.definitionName, [record]);
            }
        });

        deploymentRecordsMap.forEach((value: EnvironmentDeploymentRecord[]) => {
            value.forEach((element, index, array) => {
                element.previousBuildStartTime = ((index + 1) < array.length) ? array[index + 1].buildStartTime : undefined;
            });
        });
    }

    private getExpectedEnvironment(expectedEnvironmentName: string, allEnvironments: Environment[]): Environment {
        const expectedEnvironment = allEnvironments.filter(environment => environment.name === expectedEnvironmentName);
        if (expectedEnvironment.length !== 1) {
            // todo handle error
            console.error('Should find only one expected environment but found ', expectedEnvironment.length);
            console.error('Expected environment: ', expectedEnvironment);
        }
        return expectedEnvironment[0];
    }

    private async getEnvironmentDeploymentsRecords(recordUrl: string, authHeader: string, endSearchDateInMilliseconds: number): Promise<EnvironmentDeploymentRecord[]> {

        const continuationTokenHeaderName = 'x-ms-continuationtoken';
        let continuationTokenValue = undefined;

        const allEnvironmentDeploymentRecords: EnvironmentDeploymentRecord[] = [];

        do {
            const url = recordUrl + (continuationTokenValue ? ('&continuationToken=' + continuationTokenValue) : '');
            const environmentDeploymentRecordsResponse: Response = await fetch(url, {headers: {Authorization: authHeader}});
            continuationTokenValue = environmentDeploymentRecordsResponse.headers.get(continuationTokenHeaderName);
            const environmentDeploymentRecordsResponseBody = await environmentDeploymentRecordsResponse.json();
            const recordsNumber = environmentDeploymentRecordsResponseBody.count;
            const azureEnvironmentDeploymentRecords: OriginalEnvironmentDeploymentRecord[] = environmentDeploymentRecordsResponseBody.value;
            const environmentDeploymentRecords: EnvironmentDeploymentRecord[]
                = azureEnvironmentDeploymentRecords.map(record => AzureModelsMapper.mapAzureEnvironmentDeploymentRecordToDeploymentRecord(record));
            allEnvironmentDeploymentRecords.push(...environmentDeploymentRecords);
            const lastDeploymentRecordFinishDateInMilliseconds = new Date(environmentDeploymentRecords[recordsNumber - 1].jobFinishTime).getTime();
            if (lastDeploymentRecordFinishDateInMilliseconds < endSearchDateInMilliseconds) {
                break;
            }
        } while (continuationTokenValue);

        return allEnvironmentDeploymentRecords;
    }
}