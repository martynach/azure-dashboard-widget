import {CycleTimeInfo} from "../models/ui/CycleTimeInfo";


export interface IAzureDevopsRestApiService {
    getCycleTimeInfo(endSearchDateInMilliseconds: number): Promise<CycleTimeInfo[]>;
}