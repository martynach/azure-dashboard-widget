import {CycleTimeInfo, PullRequestInfo} from "../models/ui/CycleTimeInfo";
import {EnvironmentDeploymentRecord} from "../models/ui/EnvironmentDeploymentRecord";
import {Environment} from "../models/azure/Environment";
import {PullRequest} from "../models/PullRequest";
import {IAzureDevopsRestApiService} from "./IAzureDevopsRestApiService";
import {AzureModelsMapper} from "../models/AzureModelsMapper";
import {OriginalEnvironmentDeploymentRecord} from "../models/azure/OriginalEnvironmentDeploymentRecord";

export class AzureDevopsRestApiDeploymentRecordsBuildChangesService implements IAzureDevopsRestApiService{

    private webContext: WebContext = VSS.getWebContext();
    private ALL_ENVIRONMENTS_LIST_URL = 'https://dev.azure.com/' + this.webContext.account.name + '/' + this.webContext.project.name + '/_apis/distributedtask/environments?&api-version=6.0-preview.1';

    private authorizationHeader;
    private environment: Environment;
    private ENVIRONMENT_DEPLOYMENT_RECORDS_URL;

    constructor(private VSS_Auth_Service, private environmentName: string) {
    }

    private async setAuthorizationHeader() {
        const token = await VSS.getAccessToken();
        this.authorizationHeader = this.VSS_Auth_Service.authTokenManager.getAuthorizationHeader(token);
    }

    private async setEnvironment() {
        const environmentListResponse = await (await fetch(this.ALL_ENVIRONMENTS_LIST_URL, {headers: {Authorization: this.authorizationHeader}})).json();

        const expectedEnvironment: Environment = this.getExpectedEnvironment(this.environmentName, environmentListResponse.value);
        this.environment = expectedEnvironment;
    }

    private async setEnvironmentDeploymentRecordsUrl() {
        if(!this.environment) {
            await this.setEnvironment();
        }
        this.ENVIRONMENT_DEPLOYMENT_RECORDS_URL =
            'https://dev.azure.com/' + this.webContext.account.name
            + '/' + this.webContext.project.name + '/_apis/distributedtask/environments/'
            + this.environment.id + '/environmentdeploymentrecords?api-version=6.0-preview.1';
    }


    public async getCycleTimeInfo(endSearchDateInMilliseconds: number): Promise<CycleTimeInfo[]> {

        if (!this.authorizationHeader) {
            await this.setAuthorizationHeader();
        }

        if (!this.ENVIRONMENT_DEPLOYMENT_RECORDS_URL) {
            await this.setEnvironmentDeploymentRecordsUrl();
        }

        const environmentDeploymentRecords: EnvironmentDeploymentRecord[] =
            await this.getEnvironmentDeploymentsRecords(this.ENVIRONMENT_DEPLOYMENT_RECORDS_URL, this.authorizationHeader, endSearchDateInMilliseconds);

        const cycleTimeData: CycleTimeInfo[] = [];
        const pullRequestStartMessage: string = 'Merged PR ';

        const deploymentChangesResponsePromises: Promise<any>[] = [];

        for (let record of environmentDeploymentRecords) {
            const deploymentChangesResponsePromise = fetch(record.buildChangesUrl, {headers: {Authorization: this.authorizationHeader}});
            deploymentChangesResponsePromises.push(deploymentChangesResponsePromise);
        }

        const deploymentChangesResponses: any[] = await Promise.all(deploymentChangesResponsePromises);


        for (let index = 0; index < deploymentChangesResponses.length; index++) {
            const deploymentChangesResponse = deploymentChangesResponses[index];
            const deploymentRecord = environmentDeploymentRecords[index];


            if (!deploymentChangesResponse.ok) {
                console.error('Error when requesting changes; response status: ', deploymentChangesResponse.status);
                console.error('response: ', deploymentChangesResponse);
                // error due to azure retention time

                continue;
            }

            const deploymentChangesResponseBody = await deploymentChangesResponse.json();

            const deploymentPullRequests: PullRequest[] = deploymentChangesResponseBody.value.filter((change: PullRequest) => change.message.startsWith(pullRequestStartMessage));
            const pullRequestsInfo: PullRequestInfo[] = deploymentPullRequests.map((pullRequest: PullRequest) => {
                return {
                    completionDate: pullRequest.timestamp,
                    message: pullRequest.message,
                    location: pullRequest.location
                };
            });

            const cycleTimeInfo: CycleTimeInfo = {
                deploymentDefinitionName: deploymentRecord.definitionName,
                deploymentDate: deploymentRecord.jobFinishTime,
                pullRequestsInfo: pullRequestsInfo
            }

            cycleTimeData.push(cycleTimeInfo);

        }

        return cycleTimeData;
    }

    private getExpectedEnvironment(expectedEnvironmentName: string, allEnvironments: Environment[]): Environment {
        const expectedEnvironment = allEnvironments.filter(environment => environment.name === expectedEnvironmentName);
        if (expectedEnvironment.length !== 1) {
            // todo handle error
            console.error('Should find only one expected environment but found ', expectedEnvironment.length);
            console.error('Expected environment: ', expectedEnvironment);
        }
        return expectedEnvironment[0];
    }

    private async getEnvironmentDeploymentsRecords(recordUrl: string, authHeader: string, endSearchDateInMilliseconds: number) {

        const continuationTokenHeaderName = 'x-ms-continuationtoken';
        let continuationTokenValue = undefined;

        const allEnvironmentDeploymentRecords: EnvironmentDeploymentRecord[] = [];

        do {
            const url = recordUrl + (continuationTokenValue ? ('&continuationToken=' + continuationTokenValue) : '');
            const environmentDeploymentRecordsResponse: Response = await fetch(url, {headers: {Authorization: authHeader}});
            continuationTokenValue = environmentDeploymentRecordsResponse.headers.get(continuationTokenHeaderName);
            const environmentDeploymentRecordsResponseBody = await environmentDeploymentRecordsResponse.json();
            const recordsNumber = environmentDeploymentRecordsResponseBody.count;
            const azureEnvironmentDeploymentRecords: OriginalEnvironmentDeploymentRecord[] = environmentDeploymentRecordsResponseBody.value;
            const environmentDeploymentRecords: EnvironmentDeploymentRecord[]
                = azureEnvironmentDeploymentRecords.map(record => AzureModelsMapper.mapAzureEnvironmentDeploymentRecordToDeploymentRecord(record));
            allEnvironmentDeploymentRecords.push(...environmentDeploymentRecords);
            const lastDeploymentRecordFinishDateInMilliseconds = new Date(environmentDeploymentRecords[recordsNumber - 1].jobFinishTime).getTime();
            if (lastDeploymentRecordFinishDateInMilliseconds < endSearchDateInMilliseconds) {
                break;
            }
        } while (continuationTokenValue);

        console.log('1 allEnvironmentDeploymentRecords: ', allEnvironmentDeploymentRecords);

        return allEnvironmentDeploymentRecords;
    }
}