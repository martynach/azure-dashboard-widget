/// <reference types="vss-web-extension-sdk" />

import {CycleTimeInfo} from "./models/ui/CycleTimeInfo";
import {ChartDrawingService} from "./charts/chart-drawing.service";
import {IAzureDevopsRestApiService} from "./azure-devops-rest-api/IAzureDevopsRestApiService";
import {AzureDevopsRestApiPullRequestSortedByDatesService} from "./azure-devops-rest-api/AzureDevopsRestApiPullRequestSortedByDatesService";
import {AzureDevopsRestApiDeploymentRecordsBuildChangesService} from "./azure-devops-rest-api/AzureDevopsRestApiDeploymentRecordsBuildChangesService";
import {DateRangeService} from "./date-range.service";

VSS.init({
    explicitNotifyLoaded: true,
    usePlatformStyles: true
});

const userInput: string = 'PRODCOMM';
// const userInput: string = 'STAGE';



VSS.require(["TFS/Dashboards/WidgetHelpers", "VSS/Authentication/Services"], function (WidgetHelpers, VSS_Auth_Service) {
    WidgetHelpers.IncludeWidgetStyles();
    WidgetHelpers.IncludeWidgetConfigurationStyles();
    VSS.register("ContinuousDeliveryWidget", function () { // ContinuousDeliveryWidget - the same as id in vss-extension.json


        return { // the value returned should satisfy IWidget contract
            load: async function (widgetSettings) {

                const $title = $('h2.title');
                $title.text('Continuous Delivery Widget');
                try {



                    // retrieve data from backend
                    // const azureDevopsRestApiService: IAzureDevopsRestApiService = new AzureDevopsRestApiDeploymentRecordsBuildChangesService(VSS_Auth_Service, userInput);

                    const azureDevopsRestApiService: IAzureDevopsRestApiService = new AzureDevopsRestApiPullRequestSortedByDatesService(VSS_Auth_Service, userInput);
                    const defaultDateRange: number = 1000 * 60 * 60 * 24 * 30;
                    const today: number = Date.now();
                    const monthCycleTimeData: CycleTimeInfo[] = await azureDevopsRestApiService.getCycleTimeInfo(today - defaultDateRange);
                    console.log('monthCycleTimeData: ', monthCycleTimeData);


                    // present data on frontend
                    const chartDrawingService: ChartDrawingService = new ChartDrawingService();
                    await chartDrawingService.createChartWithDeploymentDatesOnXAxis(monthCycleTimeData);

                    const yearDateRange: number = 1000 * 60 * 60 * 24 * 365;
                    const service: IAzureDevopsRestApiService = new AzureDevopsRestApiPullRequestSortedByDatesService(VSS_Auth_Service, userInput);
                    service.getCycleTimeInfo(today - yearDateRange).then((yearTimeData: CycleTimeInfo[]) => {
                        const onDateSelect = (startDate: Date, endDate: Date) => {
                            const startTime = startDate.getTime();
                            const endTime = endDate.getTime();

                            const filteredData = yearTimeData.filter(data => {
                                const deploymentTime = new Date(data.deploymentDate).getTime();
                                return deploymentTime <= endTime && deploymentTime >= startTime;
                            });
                            chartDrawingService.updateChartWithDeploymentDatesOnXAxis(filteredData);

                        };
                        const defaultStartDate = monthCycleTimeData[0].deploymentDate; // oldest deployment
                        const dateRangeService: DateRangeService = new DateRangeService(onDateSelect, defaultStartDate, today);
                        document.getElementById('spinner').hidden = true;
                    });

                } catch (error) {
                    console.log('error: ');
                    console.error(error)
                }

                return WidgetHelpers.WidgetStatusHelper.Success();
            }
        }
    });
    VSS.register("ContinuousDeliveryWidget2", function () { // ContinuousDeliveryWidget - the same as id in vss-extension.json

        return { // the value returned should satisfy IWidget contract
            load: async function (widgetSettings) {

                const $title = $('h2.title');
                $title.text('Continuous Delivery Widget2');

                // // retrieve data from backend
                // // const azureDevopsRestApiService: IAzureDevopsRestApiService = new AzureDevopsRestApiDeploymentRecordsBuildChangesService(VSS_Auth_Service, userInput);
                // const azureDevopsRestApiService: IAzureDevopsRestApiService = new AzureDevopsRestApiPullRequestSortedByDatesService(VSS_Auth_Service, userInput);
                // const defaultDateRange: number = 1000 * 60 * 60 * 24 * 30; // todo make it configurable
                // const today: number = Date.now();
                // const cycleTimeData: CycleTimeInfo[] = await azureDevopsRestApiService.getCycleTimeInfo(today - defaultDateRange);
                //
                // // present data on frontend
                // const chartDrawingService: ChartDrawingService = new ChartDrawingService();
                // await chartDrawingService.createChartWithPullRequestDatesOnXAxis(cycleTimeData);

                return WidgetHelpers.WidgetStatusHelper.Success();
            }
        }
    });

    VSS.notifyLoadSucceeded();
});








