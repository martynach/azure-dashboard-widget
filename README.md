Based on documentation https://docs.microsoft.com/en-us/azure/devops/extend/develop/add-dashboard-widget?view=azure-devops


Packaging extension into a .vsix file
tfx extension create --manifest-globs vss-extension.json

Package and publish extension in one step
Optionally use --share-with to share your extension with one or more accounts after publishing. You'll need a personal access token, too.
tfx extension publish --manifest-globs your-manifest.json --share-with yourOrganization



How to retrieve data from Azure Devops api (AzureDevopsRestApiPullRequestSortedByDatesService.ts):
1. Get list of environments:
https://docs.microsoft.com/en-us/rest/api/azure/devops/distributedtask/environments/list?view=azure-devops-rest-6.0

2. Get environment deployment records:
https://docs.microsoft.com/en-us/rest/api/azure/devops/distributedtask/environmentdeployment%20records/list?view=azure-devops-rest-6.0
In the response object we get url to build definition -  query it to correlate deployment record with repository id.

3. List of pull requests:
https://docs.microsoft.com/en-us/rest/api/azure/devops/git/pull%20requests/get%20pull%20requests?view=azure-devops-rest-6.1

